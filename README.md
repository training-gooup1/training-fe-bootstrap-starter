# Bootstrap w/ Vite

Include [Bootstrap](https://getbootstrap.com)'s source Sass and individual JavaScript plugins with [Vite](https://vitejs.dev/).

## Origin base source
https://github.com/twbs/examples/tree/main/vite

## How to use

```sh
yarn install
yarn start
```